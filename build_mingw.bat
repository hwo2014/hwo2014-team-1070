set PL="cpp"
set BOTKEY="knf/6VCMeJYkfA"
set BOTNAME="ADT"

g++ ^
    cpp/main.cpp ^
    cpp/protocol.cpp ^
    cpp/connection.cpp ^
    cpp/game_logic.cpp ^
	cpp/Car.cpp ^
	cpp/Piece.cpp ^
	cpp/Race.cpp ^
    -std=c++11 -I cpp/jsoncons/src/ -l boost_system -l ws2_32 -o ADT