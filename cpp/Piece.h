#ifndef _PIECE_H_
#define _PIECE_H_

#include<string>

class Piece
{
private:
	int pieceID;
	double length;
	bool isSwitch;
	double angle;
	double radius;
public:
	Piece();
	~Piece();

	void setPieceID(int _pieceID);
	int getPieceID();
	void setLength(double _length);
	double getLength();
	void setIsSwitch(bool _isSwitch);
	bool getIsSwitch();
	void setAngle(double _angle);
	double getAngle();
	void setRadius(double _radius);
	double getRadius();
};

#endif
