#include "Car.h"


Car::Car()
: guideFlagPosition(10.0)
, length(40.0)
, width(20.0)
, color("red")
, name("ADT")

, throttle(0.0)
, velocity(0.0)
{
}


Car::~Car()
{
}

void Car::setGuideFlagPosition(double _guideFlagPosition){
	guideFlagPosition = _guideFlagPosition;
}

double Car::getGuideFlagPosition(){
	return guideFlagPosition;
}

void Car::setLength(double _length){
	length = _length;
}

double Car::getLength(){
	return length;
}

void Car::setWidth(double _width){
	width = _width;
}

double Car::getWidth(){
	return width;
}


void Car::setColor(std::string _color){
	color = _color;
}

std::string Car::getColor(){
	return color;
}

void Car::setName(std::string _name){
	name = _name;
}

std::string Car::getName(){
	return name;
}
