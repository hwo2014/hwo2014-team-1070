#include "game_logic.h"
#include "protocol.h"

using namespace hwo_protocol;

bool isTurboAvailable = false;
bool isTurbo34 = true;
bool isTurbo04 = true;

double fInPieceDistance = 0.0f;
double sInPieceDistance = 0.0f;
double wInPieceDistance = 0.0f;

//[ tan.truongkinh: Current status
const double PI = 3.14159265359f;

std::string myCarName;
std::string trackID;
std::string trackName;
jsoncons::json pieces;
jsoncons::json lanes;

bool isFirstPackage = true;

int prePieceIndex = 0;
double prePieceLength = 0.0f;


int curPieceIndex = 0;
bool isBend = false;
double curPieceRadius = 0.0;
double curPieceAngle = 0.0f;
double curPieceLength = 0.0f;
bool isSwitchable = false;

int curLane = 0;

double preDistance = 0.0f;
double curDistance = 0.0f;

double preVelocity = 0.0f; // Distance per tick
double curVelocity = 0.0f; // Distance per tick

double curAcceleration = 0.0f;

double curAngle = 0.0f;
double curAngularVelocity = 0.0f;

double preInPieceDistance = 0.0f;
//] tan.truongkinh

//Count Frame
int isCountFrame = 0;
int isCountFrameOnPiece = 0;

//piece
int oldPieceIndex;
double aVelocity[10000];
int isCountVelocity;

//
double Velocity(double s2, double s1);
double AccelerationTT(double v2, double v1);
double AccelerationR(double aTi, double angle);
bool CheckGoToNewPiece(int p1, int p2);

game_logic::game_logic()
//: action_map
//  {
//    { "join", &game_logic::on_join },
//    { "gameStart", &game_logic::on_game_start },
//    { "carPositions", &game_logic::on_car_positions },
//    { "crash", &game_logic::on_crash },
//    { "gameEnd", &game_logic::on_game_end },
//    { "error", &game_logic::on_error }
//  }
{
	action_map["join"] = std::bind(&game_logic::on_join, this, std::placeholders::_1);
	action_map["gameStart"] = std::bind(&game_logic::on_game_start, this, std::placeholders::_1);
	action_map["carPositions"] = std::bind(&game_logic::on_car_positions, this, std::placeholders::_1);
	action_map["crash"] = std::bind(&game_logic::on_crash, this, std::placeholders::_1);
	action_map["gameEnd"] = std::bind(&game_logic::on_game_end, this, std::placeholders::_1);
	action_map["error"] = std::bind(&game_logic::on_error, this, std::placeholders::_1);

	action_map["yourCar"] = std::bind(&game_logic::on_your_car, this, std::placeholders::_1);
	action_map["gameInit"] = std::bind(&game_logic::on_game_Init, this, std::placeholders::_1);
	action_map["spawn"] = std::bind(&game_logic::on_spawn, this, std::placeholders::_1);
	action_map["lapFinished"] = std::bind(&game_logic::on_lap_finished, this, std::placeholders::_1);
	action_map["turboAvailable"] = std::bind(&game_logic::on_turbo_available, this, std::placeholders::_1);
	action_map["turboStart"] = std::bind(&game_logic::on_turbo_start, this, std::placeholders::_1);
	action_map["turboEnd"] = std::bind(&game_logic::on_turbo_end, this, std::placeholders::_1);
	action_map["tournamentEnd"] = std::bind(&game_logic::on_tournament_end, this, std::placeholders::_1);
	action_map["finish"] = std::bind(&game_logic::on_finish, this, std::placeholders::_1);

	if (pRace == NULL)
	{
		pRace = new Race();
	}
	
}

game_logic::~game_logic()
{
	if (pRace != NULL)
	{
		delete pRace;
	}
}

game_logic::msg_vector game_logic::react(const jsoncons::json& msg)
{
	isCountFrame++;
	isCountFrameOnPiece++;

	const auto& msg_type = msg["msgType"].as<std::string>();
	const auto& data = msg["data"];
	auto action_it = action_map.find(msg_type);
	if (action_it != action_map.end())
	{
		return (action_it->second)(/*this,*/ data);
	}
	else
	{
		std::cout << "Unknown message type: " << msg_type << std::endl;

		game_logic::msg_vector tmp;
		tmp.push_back(make_ping());
		return tmp;
	}
}

game_logic::msg_vector game_logic::on_join(const jsoncons::json& data)
{
	game_logic::msg_vector tmp;
	tmp.push_back(make_ping());
	return tmp;
}

game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& data)
{
	game_logic::msg_vector tmp;

	tmp.push_back(make_throttle(1.0)); //set default thorttle 1.0 on start game
	tmp.push_back(make_ping());
	return tmp;
}

game_logic::msg_vector game_logic::on_car_positions(const jsoncons::json& data)
{
	game_logic::msg_vector tmp;
	
	//[ tan.truongkinh: My car position
	jsoncons::json myCarPosition;
	//] tan.truongkinh
	
	for (size_t i = 0; i < data.size(); ++i)
	{
		try
		{
			jsoncons::json position = data[i];
			
			//[ tan.truongkinh: Get my car position
			std::string name = position["id"]["name"].as<std::string>();
			if (name.compare(myCarName) == 0)
			{
				myCarPosition = position; // tan.truongkinh: this is my f*cking car position
			}
			//] tan.truongkinh
		}
		catch (const std::exception& e)
		{
			std::cerr << e.what() << std::endl;
		}
	}
	
	//[ tan.truongkinh: Get car status
	double angle = myCarPosition["angle"].as<double>();
	double inPieceDistance = myCarPosition["piecePosition"]["inPieceDistance"].as<double>();
	int endLane = myCarPosition["piecePosition"]["lane"]["endLaneIndex"].as<int>();
	int startLane = myCarPosition["piecePosition"]["lane"]["startLaneIndex"].as<int>();
	int lap = myCarPosition["piecePosition"]["lap"].as<int>();
	int pieceIndex = myCarPosition["piecePosition"]["pieceIndex"].as<int>();
	curLane = startLane;

	if (isFirstPackage)
	{
		isFirstPackage = false;
		curDistance = inPieceDistance;
		curPieceIndex = pieceIndex;
		// Get piece length
		if (pieces[curPieceIndex].has_member("radius"))
		{
			// Bend
			isBend = true;
			curPieceRadius = pieces[curPieceIndex].has_member("radius") ? pieces[curPieceIndex]["radius"].as<double>() : 0.0f;
			curPieceAngle = pieces[curPieceIndex].has_member("angle") ? pieces[curPieceIndex]["angle"].as<double>() : 0.0f;
			isSwitchable = pieces[curPieceIndex].has_member("switch") ? pieces[curPieceIndex]["switch"].as<bool>() : false;
			double r = curPieceRadius + lanes[curLane]["distanceFromCenter"].as<double>();
			curPieceLength = curPieceAngle * (PI/180) * r;
		}
		else
		{
			// Straight
			isBend = false;
			curPieceLength = pieces[curPieceIndex].has_member("length") ? pieces[curPieceIndex]["length"].as<int>() : 0;
			isSwitchable = pieces[curPieceIndex].has_member("switch") ? pieces[curPieceIndex]["switch"].as<bool>() : false;
			curPieceRadius = 0.0f;
			curPieceAngle = 0.0f;
		}
	}
	else
	{
		double leftOver = 0.0f;
		if (curPieceIndex != pieceIndex)
		{
			// tan.truongkinh: our car is on the new piece
			prePieceIndex = curPieceIndex;
			prePieceLength = curPieceLength;
			curPieceIndex = pieceIndex;
			// Get piece length
			if (pieces[curPieceIndex].has_member("radius"))
			{
				// Bend
				isBend = true;
				curPieceRadius = pieces[curPieceIndex].has_member("radius") ? pieces[curPieceIndex]["radius"].as<double>() : 0.0f;
				curPieceAngle = pieces[curPieceIndex].has_member("angle") ? pieces[curPieceIndex]["angle"].as<double>() : 0.0f;
				isSwitchable = pieces[curPieceIndex].has_member("switch") ? pieces[curPieceIndex]["switch"].as<bool>() : false;
				double distanceFromCenter = 0.0f;
				for (size_t i = 0; i < lanes.size(); ++i)
				{
					if (lanes[i]["index"].as<int>() == curLane)
					{
						distanceFromCenter = lanes[i]["distanceFromCenter"].as<double>();
						break;
					}
				}
				
				double r = curPieceRadius;
				if (curPieceAngle*distanceFromCenter > 0)
				{
					r = r - std::abs(distanceFromCenter);
				}
				else
				{
					r = r + std::abs(distanceFromCenter);
				}
				curPieceLength = std::abs(curPieceAngle) * (PI/180) * r;
			}
			else
			{
				// Straight
				isBend = false;
				curPieceLength = pieces[curPieceIndex].has_member("length") ? pieces[curPieceIndex]["length"].as<int>() : 0;
				isSwitchable = pieces[curPieceIndex].has_member("switch") ? pieces[curPieceIndex]["switch"].as<bool>() : false;
				curPieceRadius = 0.0f;
				curPieceAngle = 0.0f;
			}
			leftOver = prePieceLength - preInPieceDistance;
			preInPieceDistance = 0;
		}

		double d = (inPieceDistance - preInPieceDistance) + leftOver;
		preInPieceDistance = inPieceDistance;

		// Calculate distances
		preDistance = curDistance;
		curDistance = preDistance + d;

		// Calculate velocity
		preVelocity = curVelocity;
		curVelocity = d;

		// Calculate acceleration
		curAcceleration = curVelocity - preVelocity;
	}
	std::cout << "Current Distance = " << curDistance << std::endl;
	std::cout << "Current Velocity = " << curVelocity << std::endl;
	std::cout << "Current Acceleration = " << curAcceleration << std::endl;
	std::cout << "_________________________________________________" << std::endl;
	//] tan.truongkinh

	tmp.push_back(make_throttle(0.6));
	return tmp;
}

game_logic::msg_vector game_logic::on_crash(const jsoncons::json& data)
{
	std::cout << "Crashed: " << data << std::endl;
	isCountFrameOnPiece = 0;
	game_logic::msg_vector tmp;
	tmp.push_back(make_ping());
	return tmp;
}

game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& data)
{
	std::cout << "Game End: " << data << std::endl;
	game_logic::msg_vector tmp;
	tmp.push_back(make_ping());
	return tmp;
}

game_logic::msg_vector game_logic::on_error(const jsoncons::json& data)
{
	std::cout << "Error: " << data.to_string() << std::endl;
	game_logic::msg_vector tmp;
	tmp.push_back(make_ping());
	return tmp;
}

game_logic::msg_vector game_logic::on_your_car(const jsoncons::json& data)
{
	std::cout << "Your Car: " << data.to_string() << std::endl;
	myCarName = data["name"].as<std::string>();
	game_logic::msg_vector tmp;
	tmp.push_back(make_ping());
	return tmp;
}

game_logic::msg_vector game_logic::on_game_Init(const jsoncons::json& data)
{
	//[ tan.truongkinh: Parse init data
	std::cout << "Game Init: " << data << std::endl;
	jsoncons::json track = data["race"]["track"];
	trackID = track["id"].as<std::string>();
	trackName = track["name"].as<std::string>();
	jsoncons::json tmpLanes = track["lanes"];
	lanes = tmpLanes;
	jsoncons::json tmpPieces = track["pieces"];
	pieces = tmpPieces;
	for (size_t i = 0; i < pieces.size(); ++i)
	{
		jsoncons::json piece = pieces[i];
		int length = piece.has_member("length") ? piece["length"].as<int>() : 0;
		bool switchable = piece.has_member("switch") ? piece["switch"].as<bool>() : false;
		double radius = piece.has_member("radius") ? piece["radius"].as<double>() : 0.0f;
		double angle = piece.has_member("angle") ? piece["angle"].as<double>() : 0.0f;
	}
	//] tan.truongkinh

	game_logic::msg_vector tmp;
	tmp.push_back(make_ping());
	return tmp;
}

game_logic::msg_vector game_logic::on_spawn(const jsoncons::json& data)
{
	std::cout << "Spawn: " << data.to_string() << std::endl;
	game_logic::msg_vector tmp;
	tmp.push_back(make_ping());
	return tmp;
}

game_logic::msg_vector game_logic::on_lap_finished(const jsoncons::json& data)
{
	std::cout << "Lap Finished: " << data.to_string() << std::endl;
	game_logic::msg_vector tmp;
	tmp.push_back(make_ping());
	return tmp;
}

game_logic::msg_vector game_logic::on_turbo_available(const jsoncons::json& data)
{
	std::cout << "Turbo Available: " << data.to_string() << std::endl;
	game_logic::msg_vector tmp;
	isTurboAvailable = true;
	tmp.push_back(make_ping());
	return tmp;
}

game_logic::msg_vector game_logic::on_turbo_start(const jsoncons::json& data)
{
	std::cout << "Turbo Start: " << data.to_string() << std::endl;
	game_logic::msg_vector tmp;
	tmp.push_back(make_ping());
	return tmp;
}

game_logic::msg_vector game_logic::on_turbo_end(const jsoncons::json& data)
{
	std::cout << "Turbo End: " << data.to_string() << std::endl;
	game_logic::msg_vector tmp;
	tmp.push_back(make_ping());
	return tmp;
}

game_logic::msg_vector game_logic::on_tournament_end(const jsoncons::json& data)
{
	std::cout << "Tournament End: " << data.to_string() << std::endl;
	game_logic::msg_vector tmp;
	tmp.push_back(make_ping());
	return tmp;
}

game_logic::msg_vector game_logic::on_finish(const jsoncons::json& data)
{
	std::cout << "Finish: " << data.to_string() << std::endl;
	game_logic::msg_vector tmp;
	tmp.push_back(make_ping());
	return tmp;
}