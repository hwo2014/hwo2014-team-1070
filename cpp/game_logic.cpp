#include "game_logic.h"
#include "protocol.h"

using namespace hwo_protocol;

bool isTurboAvailable = false;
bool haveTurbo = false;
bool isTurbo34 = true;
bool isTurbo04 = true;

double fInPieceDistance = 0.0f;
double sInPieceDistance = 0.0f;
double wInPieceDistance = 0.0f;

//[ tan.truongkinh: Current status
const double PI = 3.14159265359f;

std::string myCarName;
std::string trackID;
std::string trackName;
jsoncons::json pieces;
jsoncons::json lanes;

bool isFirstPackage = true;

int prePieceIndex = 0;
double prePieceLength = 0.0f;


int curPieceIndex = 0;
bool isBend = false;
double curPieceRadius = 0.0;
double curPieceAngle = 0.0f;
double curPieceLength = 0.0f;
bool isSwitchable = false;

int curLane = 0;

double preDistance = 0.0f;
double curDistance = 0.0f;

double preVelocity = 0.0f; // Distance per tick
double curVelocity = 0.0f; // Distance per tick

double curAcceleration = 0.0f;

double curAngle = 0.0f;
double curAngularVelocity = 0.0f;

double preInPieceDistance = 0.0f;

int nextBendIndex;
double curThrottle = 1.0f;
//] tan.truongkinh

//[duytran
bool checkAngleBT = true;

int LaneSwitchCount = 0;
int aLaneSwitch[100];

int Lane2BendCount = 0;
int aLane2Bend[100];
double Distance2Bend = 0.0f;
double Distance2Straight = 0.0f;

double v0 = 0.0f;
double v1 = 0.0f;
double v2 = 0.0f;
double v3 = 0.0f;

double mk = 0.0f;
double mm = 0.0f;
double mtv = 0.0f;
double mvt = 0.0f;
int mt = 0;
double mdt = 0.0f;
double mh = 0.0f;
double mc = 0.0f;
double mv = 0.0f;

bool firstStep3 = false;
//]duytran


//Count Frame
int isCountFrame = 0;
int isCountFrameOnPiece = 0;

//piece
int oldPieceIndex;
double aVelocity[10000];
int isCountVelocity;

//
double Velocity(double s2, double s1);
double AccelerationTT(double v2, double v1);
double AccelerationR(double aTi, double angle);
bool CheckGoToNewPiece(int p1, int p2);

game_logic::game_logic()
//: action_map
//  {
//    { "join", &game_logic::on_join },
//    { "gameStart", &game_logic::on_game_start },
//    { "carPositions", &game_logic::on_car_positions },
//    { "crash", &game_logic::on_crash },
//    { "gameEnd", &game_logic::on_game_end },
//    { "error", &game_logic::on_error }
//  }
{
	action_map["join"] = std::bind(&game_logic::on_join, this, std::placeholders::_1);
	action_map["gameStart"] = std::bind(&game_logic::on_game_start, this, std::placeholders::_1);
	action_map["carPositions"] = std::bind(&game_logic::on_car_positions, this, std::placeholders::_1);
	action_map["crash"] = std::bind(&game_logic::on_crash, this, std::placeholders::_1);
	action_map["gameEnd"] = std::bind(&game_logic::on_game_end, this, std::placeholders::_1);
	action_map["error"] = std::bind(&game_logic::on_error, this, std::placeholders::_1);

	action_map["yourCar"] = std::bind(&game_logic::on_your_car, this, std::placeholders::_1);
	action_map["gameInit"] = std::bind(&game_logic::on_game_Init, this, std::placeholders::_1);
	action_map["spawn"] = std::bind(&game_logic::on_spawn, this, std::placeholders::_1);
	action_map["lapFinished"] = std::bind(&game_logic::on_lap_finished, this, std::placeholders::_1);
	action_map["turboAvailable"] = std::bind(&game_logic::on_turbo_available, this, std::placeholders::_1);
	action_map["turboStart"] = std::bind(&game_logic::on_turbo_start, this, std::placeholders::_1);
	action_map["turboEnd"] = std::bind(&game_logic::on_turbo_end, this, std::placeholders::_1);
	action_map["tournamentEnd"] = std::bind(&game_logic::on_tournament_end, this, std::placeholders::_1);
	action_map["finish"] = std::bind(&game_logic::on_finish, this, std::placeholders::_1);
	
	MyCar = new Car();
}

game_logic::~game_logic()
{
	delete MyCar;
}

game_logic::msg_vector game_logic::react(const jsoncons::json& msg)
{
	isCountFrame++;
	isCountFrameOnPiece++;

	const auto& msg_type = msg["msgType"].as<std::string>();
	const auto& data = msg["data"];
	auto action_it = action_map.find(msg_type);
	if (action_it != action_map.end())
	{
		return (action_it->second)(/*this,*/ data);
	}
	else
	{
		std::cout << "Unknown message type: " << msg_type << std::endl;

		game_logic::msg_vector tmp;
		tmp.push_back(make_ping());
		return tmp;
	}
}

game_logic::msg_vector game_logic::on_join(const jsoncons::json& data)
{
	std::cout << "Join: " << data << std::endl;

	game_logic::msg_vector tmp;
	tmp.push_back(make_throttle(curThrottle));
	//tmp.push_back(make_ping());
	return tmp;
}

game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& data)
{
	std::cout << "on_game_start: " << data << std::endl;
	game_logic::msg_vector tmp;
	curThrottle = 1.0f;
	tmp.push_back(make_throttle(curThrottle)); //set default thorttle 1.0 on start game
	tmp.push_back(make_ping());
	return tmp;
}

game_logic::msg_vector game_logic::on_car_positions(const jsoncons::json& data)
{
	game_logic::msg_vector tmp;

	//[ tan.truongkinh: My car position
	jsoncons::json myCarPosition;
	//] tan.truongkinh

	for (size_t i = 0; i < data.size(); ++i)
	{
		try
		{
			jsoncons::json position = data[i];

			//[ tan.truongkinh: Get my car position
			std::string name = position["id"]["name"].as<std::string>();
			if (name.compare(myCarName) == 0)
			{
				myCarPosition = position; // tan.truongkinh: this is my f*cking car position
			}
			//] tan.truongkinh
		}
		catch (const std::exception& e)
		{
			std::cerr << e.what() << std::endl;
		}
	}

	//[ tan.truongkinh: Get car status
	double angle = myCarPosition["angle"].as<double>();
	double inPieceDistance = myCarPosition["piecePosition"]["inPieceDistance"].as<double>();
	int endLane = myCarPosition["piecePosition"]["lane"]["endLaneIndex"].as<int>();
	int startLane = myCarPosition["piecePosition"]["lane"]["startLaneIndex"].as<int>();
	int lap = myCarPosition["piecePosition"]["lap"].as<int>();
	int pieceIndex = myCarPosition["piecePosition"]["pieceIndex"].as<int>();
	curLane = startLane;

	if (isFirstPackage)
	{
		isFirstPackage = false;
		curDistance = inPieceDistance;
		curPieceIndex = pieceIndex;
		// Get piece length
		if (pieces[curPieceIndex].has_member("radius"))
		{
			// Bend
			isBend = true;
			curPieceRadius = pieces[curPieceIndex].has_member("radius") ? pieces[curPieceIndex]["radius"].as<double>() : 0.0f;
			curPieceAngle = pieces[curPieceIndex].has_member("angle") ? pieces[curPieceIndex]["angle"].as<double>() : 0.0f;
			isSwitchable = pieces[curPieceIndex].has_member("switch") ? pieces[curPieceIndex]["switch"].as<bool>() : false;
			double r = curPieceRadius + lanes[curLane]["distanceFromCenter"].as<double>();
			curPieceLength = curPieceAngle * (PI / 180) * r;
		}
		else
		{
			// Straight
			isBend = false;
			curPieceLength = pieces[curPieceIndex].has_member("length") ? pieces[curPieceIndex]["length"].as<int>() : 0;
			isSwitchable = pieces[curPieceIndex].has_member("switch") ? pieces[curPieceIndex]["switch"].as<bool>() : false;
			curPieceRadius = 0.0f;
			curPieceAngle = 0.0f;
		}
	}
	else
	{
		double leftOver = 0.0f;
		double LeftOver = 0.0f;
		if (curPieceIndex != pieceIndex)
		{
			// tan.truongkinh: our car is on the new piece
			prePieceIndex = curPieceIndex;
			prePieceLength = curPieceLength;
			curPieceIndex = pieceIndex;
			// Get piece length
			if (pieces[curPieceIndex].has_member("radius"))
			{
				// Bend
				isBend = true;
				curPieceRadius = pieces[curPieceIndex].has_member("radius") ? pieces[curPieceIndex]["radius"].as<double>() : 0.0f;
				curPieceAngle = pieces[curPieceIndex].has_member("angle") ? pieces[curPieceIndex]["angle"].as<double>() : 0.0f;
				isSwitchable = pieces[curPieceIndex].has_member("switch") ? pieces[curPieceIndex]["switch"].as<bool>() : false;
				double distanceFromCenter = 0.0f;
				for (size_t i = 0; i < lanes.size(); ++i)
				{
					if (lanes[i]["index"].as<int>() == curLane)
					{
						distanceFromCenter = lanes[i]["distanceFromCenter"].as<double>();
						break;
					}
				}

				double r = curPieceRadius;
				if (curPieceAngle*distanceFromCenter > 0)
				{
					r = r - std::abs(distanceFromCenter);
				}
				else
				{
					r = r + std::abs(distanceFromCenter);
				}
				curPieceLength = std::abs(curPieceAngle) * (PI / 180) * r;
			}
			else
			{
				// Straight
				isBend = false;
				curPieceLength = pieces[curPieceIndex].has_member("length") ? pieces[curPieceIndex]["length"].as<int>() : 0;
				isSwitchable = pieces[curPieceIndex].has_member("switch") ? pieces[curPieceIndex]["switch"].as<bool>() : false;
				curPieceRadius = 0.0f;
				curPieceAngle = 0.0f;
			}
			leftOver = prePieceLength - preInPieceDistance;
			preInPieceDistance = 0;
		}

		double d = (inPieceDistance - preInPieceDistance) + leftOver;
		preInPieceDistance = inPieceDistance;

		// Calculate distances
		preDistance = curDistance;
		curDistance = preDistance + d;

		// Calculate velocity
		preVelocity = curVelocity;
		curVelocity = d;

		// Calculate acceleration
		curAcceleration = curVelocity - preVelocity;

		//[duytran Distance2Bend
		LeftOver = pPiece[pieceIndex].getLength() - inPieceDistance;
		Distance2Bend = 0.0f;
		if (CheckIsStraight(pieceIndex))
		{
			//[ tan.truongkinh: Count pieces to the next bend
			Distance2Bend += LeftOver;
			int count = 0;
			int total = pieces.size();
			int i = (curPieceIndex + 1) % total;
			while (count < total)
			{
				if (pieces[i].has_member("radius"))
				{
					nextBendIndex = i;
					break;
				}
				Distance2Bend += pPiece[i].getLength();
				i = (i + 1) % total;
				count++;
			}
			//] tan.truongkinh
		}
		//Angle * (PI / 180) * Radius 
		double LeftOverS = ((pPiece[pieceIndex].getAngle() < 0) ? pPiece[pieceIndex].getAngle()* -1 : pPiece[pieceIndex].getAngle()) * (PI / 180) * pPiece[pieceIndex].getRadius();
		Distance2Straight = 0.0f;
		if (CheckIsBend(pieceIndex))
		{
			Distance2Straight += LeftOverS - inPieceDistance;
			int count = 0;
			int total = pieces.size();
			int i = (curPieceIndex + 1) % total;
			while (count < total)
			{
				if (!pieces[i].has_member("radius"))
				{
					//nextBendIndex = i;
					break;
				}
				Distance2Straight += LeftOverS;
				i = (i + 1) % total;
				count++;
			}
			//] tan.truongkinh
		}
		//]duytran

	}

	if (isBend)
	{
		if (curVelocity < 6.27729f)
		{
			double delta = curVelocity*(-0.4f / 6.27729f) + 0.4f;
			if (curPieceRadius <= 100.f)
			{
				delta = delta / 3.0f;
			}
			else
			{
				delta = delta / 2.0f;
			}
			curThrottle = 0.7f + delta;
			if (curThrottle > 1.0f)
			{
				curThrottle = 1.0f;
			}
		}
		if (curVelocity > 6.27729f)
		{
			{
				curThrottle = 0.1f;
			}
		}
		else
		{
			curThrottle = 0.65f;
		}
	}
	else
	{
		if (Distance2Bend <= 100.0f)
		{
			if (curVelocity < 6.27729f)
			{
				curThrottle = 0.9f;
			}
			else
			{
				curThrottle = 0.0f;
			}
		}
		else
		{
			curThrottle = 1.0f;
		}
	}


	if (curVelocity > 7 && Distance2Bend < 390 && haveTurbo)
	{
		std::cout << "stop------------------------.0.0: " << haveTurbo << std::endl;
		if (Distance2Bend <= 0)
		{
			haveTurbo = false;
		}
		curThrottle = 0.0f;
	}
	/*else
	{
		std::cout << "nm------------------------nm: " << haveTurbo << std::endl;
		haveTurbo = false;
	}*/
	

	//[duy turbo
	if (isTurboAvailable && Distance2Bend > 400)
	{
		std::cout << "turbo------------------------" << std::endl;
		tmp.push_back(make_turboAvailable());
		isTurboAvailable = false;
	}

	if (pPiece[pieceIndex + 1].getIsSwitch() && pPiece[pieceIndex + 1].getAngle() > 0/* && Distance2Bend > 0*/)
	{
		std::cout << "switch right ---------------" << std::endl;
		tmp.push_back(make_switchLane(1));
	}
	else if (pPiece[pieceIndex + 1].getIsSwitch() && pPiece[pieceIndex + 1].getAngle() < 0/* && Distance2Straight > 0*/)
	{
		std::cout << "switch left ---------------" << std::endl;
		tmp.push_back(make_switchLane(0));
	}
	//]duy
	
	std::cout << "Car angle = " << angle << std::endl;
	std::cout << "Current Distance = " << curDistance << std::endl;
	std::cout << "Current Velocity = " << curVelocity << std::endl;
	std::cout << "Current Acceleration = " << curAcceleration << std::endl;
	std::cout << "Piece = " << pieceIndex << " -- " << "Distance2Bend = " << Distance2Bend << std::endl;
	std::cout << "Piece = " << pieceIndex << " -- " << "Distance2Straight = " << Distance2Straight << std::endl;
	std::cout << "Throttle = " << curThrottle << std::endl;
	std::cout << "_________________________________________________" << std::endl;
	//] tan.truongkinh

	tmp.push_back(make_throttle(curThrottle));
	return tmp;
}

game_logic::msg_vector game_logic::on_crash(const jsoncons::json& data)
{
	std::cout << "Crashed: " << data << std::endl;
	curDistance = curDistance - preInPieceDistance;
	preVelocity = 0.0f;
	isCountFrameOnPiece = 0;
	game_logic::msg_vector tmp;
	tmp.push_back(make_throttle(curThrottle));
	//tmp.push_back(make_ping());
	return tmp;
}

game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& data)
{
	std::cout << "Game End: " << data << std::endl;
	game_logic::msg_vector tmp;
	//tmp.push_back(make_ping());
	tmp.push_back(make_throttle(curThrottle));
	return tmp;
}

game_logic::msg_vector game_logic::on_error(const jsoncons::json& data)
{
	std::cout << "Error: " << data.to_string() << std::endl;
	game_logic::msg_vector tmp;
	//tmp.push_back(make_ping());
	tmp.push_back(make_throttle(curThrottle));
	return tmp;
}

game_logic::msg_vector game_logic::on_your_car(const jsoncons::json& data)
{
	std::cout << "Your Car: " << data.to_string() << std::endl;
	myCarName = data["name"].as<std::string>();
	game_logic::msg_vector tmp;
	//tmp.push_back(make_ping());
	tmp.push_back(make_throttle(curThrottle));
	return tmp;
}

game_logic::msg_vector game_logic::on_game_Init(const jsoncons::json& data)
{
	//[ tan.truongkinh: Parse init data
	std::cout << "Game Init: " << data << std::endl;

	jsoncons::json cars = data["race"]["cars"];
	for (size_t j = 0; j < cars.size(); ++j)
	{
		jsoncons::json id = cars[j]["id"];
		std::string color = id.has_member("color") ? id["color"].as<std::string>() : "";
		std::string name = id.has_member("name") ? id["name"].as<std::string>() : "";
		if (name.compare(myCarName) == 0)
		{
			jsoncons::json dimensions = cars[j]["dimensions"];
			double guideFlagPosition = dimensions.has_member("guideFlagPosition") ? dimensions["guideFlagPosition"].as<double>() : 0.0;
			int length = dimensions.has_member("length") ? dimensions["length"].as<int>() : 0;
			int width = dimensions.has_member("width") ? dimensions["width"].as<int>() : 0;
			MyCar->setName(name);
			MyCar->setColor(color);
			MyCar->setGuideFlagPosition(guideFlagPosition);
			MyCar->setWidth(width);
			MyCar->setLength(length);

			std::cout << "------ guideFlagPosition: " << MyCar->getGuideFlagPosition() << std::endl;
			std::cout << "------ length: " << MyCar->getLength() << std::endl;
			std::cout << "------ width: " << MyCar->getWidth() << std::endl << std::endl;

			std::cout << "------ color: " << MyCar->getColor() << std::endl;
			std::cout << "------ name: " << MyCar->getName() << std::endl << std::endl;
			std::cout << "------------------------ " << std::endl;
		}
		
	}

	jsoncons::json track = data["race"]["track"];
	trackID = track["id"].as<std::string>();
	trackName = track["name"].as<std::string>();
	jsoncons::json tmpLanes = track["lanes"];
	lanes = tmpLanes;
	jsoncons::json tmpPieces = track["pieces"];
	pieces = tmpPieces;

	std::cout << "Game Init - pieces.size(): " << pieces.size() << std::endl;

	for (size_t i = 0; i < pieces.size(); ++i)
	{
		jsoncons::json piece = pieces[i];
		int length = piece.has_member("length") ? piece["length"].as<int>() : 0;
		bool switchable = piece.has_member("switch") ? piece["switch"].as<bool>() : false;
		double radius = piece.has_member("radius") ? piece["radius"].as<double>() : 0.0f;
		double angle = piece.has_member("angle") ? piece["angle"].as<double>() : 0.0f;

		//duytran[
		if (switchable)
		{
			aLaneSwitch[LaneSwitchCount++] = i;
		}

		if (angle != 0)
		{
			aLane2Bend[Lane2BendCount++] = i;
		}

		pPiece[i].setPieceID(i);
		pPiece[i].setLength(length);
		pPiece[i].setIsSwitch(switchable);
		pPiece[i].setRadius(radius);
		pPiece[i].setAngle(angle);
		//]duytran

	}
	//] tan.truongkinh

	//duytran[
	for (int ccc = 0; ccc < Lane2BendCount; ccc++)
	{
		std::cout << "-----------------------: " << std::endl;
		std::cout << aLane2Bend[ccc] << " - ";
		std::cout << "-----------------------: " << std::endl;
	}

	/*for (size_t j = 0; j < pieces.size(); ++j)
	{
		std::cout << "-----------------------: " << std::endl;
		std::cout << "ID: " << pPiece[j].getPieceID() << std::endl;
		std::cout << "length: " << pPiece[j].getLength() << std::endl;
		std::cout << "switchable: " << pPiece[j].getIsSwitch() << std::endl;
		std::cout << "radius: " << pPiece[j].getRadius() << std::endl;
		std::cout << "angle: " << pPiece[j].getAngle() << std::endl;
		std::cout << "-----------------------: " << std::endl;
	}*/
	//]duytran[

	game_logic::msg_vector tmp;
	//tmp.push_back(make_ping());
	tmp.push_back(make_throttle(curThrottle));
	return tmp;
}

game_logic::msg_vector game_logic::on_spawn(const jsoncons::json& data)
{
	std::cout << "Spawn: " << data.to_string() << std::endl;
	game_logic::msg_vector tmp;
	//tmp.push_back(make_ping());
	tmp.push_back(make_throttle(curThrottle));
	return tmp;
}

game_logic::msg_vector game_logic::on_lap_finished(const jsoncons::json& data)
{
	std::cout << "Lap Finished: " << data.to_string() << std::endl;
	game_logic::msg_vector tmp;
	//tmp.push_back(make_ping());
	tmp.push_back(make_throttle(curThrottle));
	return tmp;
}

game_logic::msg_vector game_logic::on_turbo_available(const jsoncons::json& data)
{
	std::cout << "Turbo Available: " << data.to_string() << std::endl;
	game_logic::msg_vector tmp;
	isTurboAvailable = true;
	//tmp.push_back(make_ping());
	tmp.push_back(make_throttle(curThrottle));
	return tmp;
}

game_logic::msg_vector game_logic::on_turbo_start(const jsoncons::json& data)
{
	std::cout << "Turbo Start: " << data.to_string() << std::endl;
	game_logic::msg_vector tmp;
	haveTurbo = true;
	//tmp.push_back(make_ping());
	tmp.push_back(make_throttle(curThrottle));
	return tmp;
}

game_logic::msg_vector game_logic::on_turbo_end(const jsoncons::json& data)
{
	std::cout << "Turbo End: " << data.to_string() << std::endl;
	game_logic::msg_vector tmp;
	//tmp.push_back(make_ping());
	tmp.push_back(make_throttle(curThrottle));
	return tmp;
}

game_logic::msg_vector game_logic::on_tournament_end(const jsoncons::json& data)
{
	std::cout << "Tournament End: " << data.to_string() << std::endl;
	game_logic::msg_vector tmp;
	//tmp.push_back(make_ping());
	tmp.push_back(make_throttle(curThrottle));
	return tmp;
}

game_logic::msg_vector game_logic::on_finish(const jsoncons::json& data)
{
	std::cout << "Finish: " << data.to_string() << std::endl;
	game_logic::msg_vector tmp;
	//tmp.push_back(make_ping());
	tmp.push_back(make_throttle(curThrottle));
	return tmp;
}

//duytran[
int game_logic::CountLane2Bend(int laneID)
{
	for (int i = 0; i < Lane2BendCount; i++)
	{
		if (laneID < aLane2Bend[i])
		{
			return aLane2Bend[i] - laneID - 1;
		}
		else
			return -1; //Error
	}
	return -1; //Error
}

bool game_logic::CheckIsStraight(int laneID)
{
	return  (pPiece[laneID].getAngle() == 0) ? 1 : 0;
}

bool game_logic::CheckIsBend(int laneID)
{
	return  (pPiece[laneID].getAngle() != 0) ? 1 : 0;
}

int game_logic::CheckAngleBendStraight(int laneID)
{
	if (pPiece[laneID].getAngle() > 0)
	{
		return 1;
	}
	else if (pPiece[laneID].getAngle() < 0)
	{
		return 0;
	}
	else
	{
		return -1;
	}
}



double game_logic::k(double v2, double v1, double h){
	return (v1 - (v2 - v1)) / v1 * 2 * h;
}

double game_logic::m(double v3, double v2, double k, double h){
	return 1.0 / (log((v3 - (h / k)) / (v2 - (h / k))) / (-k));
}

//terminal velocity
double game_logic::tv(double h, double k){
	return h / k;
}
double game_logic::vt(double v0, double h, double k, double m, int t){
	//v0 = real v
	return (v0 - (h / k)) * pow(eLn , ((-k * t) / m) + (h / k));
}

int game_logic::t(double v, double h, double k, double m, double v0){
	return (int)round((log((v - (h / k)) / (v0 - (h / k))) * m) / (-k));
}

double game_logic::dt(double m, double k, double v0, int t, double h, double d0){
	return (m / k) * (v0 - (h / k)) * (1.0 - pow(eLn, ((-k*t) / m)) + (h / k) * t + d0);
}

double game_logic::h(double m, double k, double v0, double vt, int t){
	return (k * (vt * pow(eLn, ((k * t) / m)) - v0) / (pow(eLn, ((k * t) / m)) - 1.0));
}

double game_logic::c(double v, double r){
	return v*v * r;
}

double game_logic::v(double c, double r){
	return sqrt(c * r);
}

//]duytran