#ifndef HWO_GAME_LOGIC_H
#define HWO_GAME_LOGIC_H

#include <string>
#include <vector>
#include <map>
#include <functional>
#include <iostream>
#include <jsoncons/json.hpp>

#include "Piece.h"
#include "Car.h"
#define MAXPIECE 200
#define eLn 2.71828182845904523536028747135266250

//#include "Utility.cpp"

class game_logic
{
public:
	typedef std::vector<jsoncons::json> msg_vector;

	game_logic();
	~game_logic();
	msg_vector react(const jsoncons::json& msg);

private:
	typedef std::function<msg_vector(/*game_logic*, */const jsoncons::json&)> action_fun;
	/*const*/ std::map<std::string, action_fun> action_map;

	msg_vector on_join(const jsoncons::json& data);
	msg_vector on_game_start(const jsoncons::json& data);
	msg_vector on_car_positions(const jsoncons::json& data);
	msg_vector on_crash(const jsoncons::json& data);
	msg_vector on_game_end(const jsoncons::json& data);
	msg_vector on_error(const jsoncons::json& data);
	
	msg_vector on_your_car(const jsoncons::json& data);
	msg_vector on_game_Init(const jsoncons::json& data);
	msg_vector on_spawn(const jsoncons::json& data);
	msg_vector on_lap_finished(const jsoncons::json& data);
	msg_vector on_turbo_available(const jsoncons::json& data);
	msg_vector on_turbo_start(const jsoncons::json& data);
	msg_vector on_turbo_end(const jsoncons::json& data);
	msg_vector on_tournament_end(const jsoncons::json& data);
	msg_vector on_finish(const jsoncons::json& data);

private:
	Piece pPiece[MAXPIECE];
	Car* MyCar;
public:
	int CountLane2Bend(int laneID);
	bool CheckIsStraight(int laneID);
	bool CheckIsBend(int laneID);
	int CheckAngleBendStraight(int laneID);

	double k(double v2, double v1, double h);
	double m(double v3, double v2, double k, double h);
	double tv(double throttle, double k);
	double vt(double v0, double h, double k, double m, int t);
	int t(double v, double h, double k, double m, double v0);
	double dt(double m, double k, double v0, int t, double h, double d0);
	double h(double m, double k, double v0, double vt, int t);
	double c(double v, double r);
	double v(double c, double r);
};

#endif
