#ifndef _RACE_H_
#define _RACE_H_

#include "Car.h"
#include "Piece.h"

#define NUMPIECE 200

class Race
{
private:
	Piece aPiece[NUMPIECE];

	int length;
	int width;
	double guideFlagPosition;

	Car* pMyCar;
	Car* pEmenyCar;
public:
	Race();
	~Race();

	void setLength(int _length);
	int getLength();
	void setWidth(int _width);
	int getWidth();
	void setGuideFlagPosition(double guideFlagPosition);
	double getGuideFlagPosition();
};

#endif