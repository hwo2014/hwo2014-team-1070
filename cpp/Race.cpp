#include "Race.h"


Race::Race()
: length(0)
, width(0)
, guideFlagPosition(0)
{
	if (pMyCar == NULL)
	{
		pMyCar = new Car();
	}

	if (pEmenyCar == NULL)
	{
		pEmenyCar = new Car();
	}
}


Race::~Race()
{
	if (pMyCar != NULL)
	{
		delete pMyCar;
	}

	if (pEmenyCar != NULL)
	{
		delete pEmenyCar;
	}
}

void Race::setLength(int _length){
	length = _length;
}

int Race::getLength(){
	return length;
}

void Race::setWidth(int _width)
{
	width = _width;
}

int Race::getWidth()
{
	return width;
}

void Race::setGuideFlagPosition(double _guideFlagPosition)
{
	guideFlagPosition = _guideFlagPosition;
}

double Race::getGuideFlagPosition()
{
	return guideFlagPosition;
}