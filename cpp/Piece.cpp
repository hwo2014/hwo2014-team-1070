#include "Piece.h"


Piece::Piece()
: pieceID(0)
, length(0.0)
, isSwitch(false)
, angle(0.0)
, radius(0.0)
{
}


Piece::~Piece()
{
}

void Piece::setPieceID(int _pieceID){
	pieceID = _pieceID;
}

int Piece::getPieceID(){
	return pieceID;
}

void Piece::setLength(double _length){
	length = _length;
}

double Piece::getLength(){
	return length;
}

void Piece::setIsSwitch(bool _isSwitch){
	isSwitch = _isSwitch;
}

bool Piece::getIsSwitch(){
	return isSwitch;
}

void Piece::setAngle(double _angle){
	angle = _angle;
}

double Piece::getAngle(){
	return angle;
}

void Piece::setRadius(double _radius){
	radius = _radius;
}

double Piece::getRadius(){
	return radius;
}