#ifndef _CAR_H_
#define _CAR_H_

#include<string>

class Car
{
private:
	double guideFlagPosition;
	double length;
	double width;
	std::string color;
	std::string name;

	double throttle;
	double velocity;

public:
	Car();
	~Car();

	void setGuideFlagPosition(double _guideFlagPosition);
	double getGuideFlagPosition();
	void setLength(double _length);
	double getLength();
	void setWidth(double _width);
	double getWidth();

	void setColor(std::string _color);
	std::string getColor();
	void setName(std::string _name);
	std::string getName();

	void setThrottle(double _throttle)
	{
		throttle = _throttle;
	}
	double getThrottle()
	{
		return throttle;
	}

	void setVelocity(double _velocity)
	{
		velocity = _velocity;
	}

	double getVelocity()
	{
		return velocity;
	}
};

#endif
