* How to build on windows.
Using Cygwin to build this project.
Ex:
mypc /cygdrive/e/Projects/AI/Car2014/hwo2014-team-x
$ ./build
Note:
Looks like Notepad++ by default has EOL settings so that only LF is placed at the end of each line.  Which is fine in Notepad++ but not when you have to open the file in plain old Notepad--the lines run together. 
To change the EOL settings, go to Edit->EOL Conversion, and set it to "Windows Format" instead of "Unix Format".

// Using vs2012
- Install boost-binaries version 1.55.0 (boost_1_55_0-msvc-11.0-32.exe - using for x64)
link: http://sourceforge.net/projects/boost/files/boost-binaries/1.55.0/

- Extrack boost libs and import on project files:
+ C/C++ - Additional Include Directories (Ex: C:\local\boost_1_55_0, libboost_system-vc110-mt-gd-1_55)
+ Copy 3 libs files to libs folder (libboost_date_time-vc110-mt-gd-1_55, libboost_regex-vc110-mt-gd-1_55)
+ Set on Linker - All Options - Additional Library Directories (Ex: $(MSBuildProjectDirectory)\src\libs)
- Build & Run project on .\trunk\cppproject\car2014\