#include "game_logic.h"
#include "protocol.h"

using namespace hwo_protocol;

game_logic::game_logic()
  //: action_map
  //  {
  //    { "join", &game_logic::on_join },
  //    { "gameStart", &game_logic::on_game_start },
  //    { "carPositions", &game_logic::on_car_positions },
  //    { "crash", &game_logic::on_crash },
  //    { "gameEnd", &game_logic::on_game_end },
  //    { "error", &game_logic::on_error }
  //  }
{
    action_map["join"] = std::bind(&game_logic::on_join, this, std::placeholders::_1);
    action_map["gameStart"] = std::bind(&game_logic::on_game_start, this, std::placeholders::_1);
    action_map["carPositions"] = std::bind(&game_logic::on_car_positions, this, std::placeholders::_1);
    action_map["crash"] = std::bind(&game_logic::on_crash, this, std::placeholders::_1);
    action_map["gameEnd"] = std::bind(&game_logic::on_game_end, this, std::placeholders::_1);
    action_map["error"] = std::bind(&game_logic::on_error, this, std::placeholders::_1);
}

game_logic::msg_vector game_logic::react(const jsoncons::json& msg)
{
  const auto& msg_type = msg["msgType"].as<std::string>();
  const auto& data = msg["data"];
  auto action_it = action_map.find(msg_type);
  if (action_it != action_map.end())
  {
    return (action_it->second)(/*this,*/ data);
  }
  else
  {
    std::cout << "Unknown message type: " << msg_type << std::endl;
    //return { make_ping() };

    game_logic::msg_vector tmp;
    tmp.push_back(make_ping());
    return tmp;
  }
}

game_logic::msg_vector game_logic::on_join(const jsoncons::json& data)
{
  std::cout << "Joined" << std::endl;
  //return { make_ping() };

  game_logic::msg_vector tmp;
  tmp.push_back(make_ping());
  return tmp;
}

game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& data)
{
  std::cout << "Race started" << std::endl;
  //return { make_ping() };

  game_logic::msg_vector tmp;
  tmp.push_back(make_ping());
  return tmp;
}

game_logic::msg_vector game_logic::on_car_positions(const jsoncons::json& data)
{
	//return { make_throttle(0.5) };
	game_logic::msg_vector tmp;
	double carThrottle = 0.0;
	std::cout << "on_car_positions Race data 111111111111"<< data << std::endl;
	for (size_t i = 0; i < data.size(); ++i)
	{
		try
		{
			jsoncons::json position = data[i];
			double angle = position["angle"].as<double>();
			int endLane = position["piecePosition"]["lane"]["endLaneIndex"].as<int>();
			int startLane = position["piecePosition"]["lane"]["startLaneIndex"].as<int>();

			int pieceIndex = position["piecePosition"]["pieceIndex"].as<int>();

			std::cout << "----angle: " << angle << std::endl;
			std::cout << "----endLane: " << endLane << std::endl;
			std::cout << "----startLane: " << startLane << std::endl;
			std::cout << "----pieceIndex: " << pieceIndex << std::endl;

			/*if(endLane == 0 && startLane == 0)
			{
				tmp.push_back(make_switchLane(1));
			}
			else
			{
				tmp.push_back(make_switchLane(0));
			}*/

			//cheat map
			if(	
				(pieceIndex >= 33 && pieceIndex <= 39) ||
				(pieceIndex >= 0 && pieceIndex <= 2) ||
				(pieceIndex >= 22 && pieceIndex <= 24)
				)
			{
				carThrottle = 1.0;
			}
			else if(pieceIndex == 3 || pieceIndex == 12 || pieceIndex == 25)
			{
				carThrottle = 0.0;
			}
			else if(pieceIndex == 4 ||
					(pieceIndex >= 13 && pieceIndex <=17)
					)
			{
				carThrottle = 0.6;
			}
			else if(pieceIndex >= 5 && pieceIndex <= 6)
			{
				carThrottle = 0.7;
			}
			else if(pieceIndex >= 7 && pieceIndex <= 11)
			{
				carThrottle = 1.0;
			}
			else if(pieceIndex >= 18 && pieceIndex <= 21)
			{
				carThrottle = 0.75;
			}
			else if(pieceIndex >= 26 && pieceIndex <= 31)
			{
				carThrottle = 0.7;
			}
			else
			{
				carThrottle = 0.5;
			}
			
			tmp.push_back(make_throttle(carThrottle));
			std::cout << "----carThrottle: " << carThrottle << std::endl;
			return tmp;
		}
		catch (const std::exception& e)
		{
			std::cerr << e.what() << std::endl;
		}
	}

	
	//tmp.push_back(make_throttle(0.6));
	return tmp;
}

game_logic::msg_vector game_logic::on_crash(const jsoncons::json& data)
{
	std::cout << "on_crash Race data" << data << std::endl;
  std::cout << "Someone crashed" << std::endl;
  //return { make_ping() };

  game_logic::msg_vector tmp;
  tmp.push_back(make_ping());
  return tmp;
}

game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& data)
{
	std::cout << "on_game_end Race data" << data << std::endl;
  std::cout << "Race ended" << std::endl;
  //return { make_ping() };

  game_logic::msg_vector tmp;
  tmp.push_back(make_ping());
  return tmp;
}

game_logic::msg_vector game_logic::on_error(const jsoncons::json& data)
{
  std::cout << "Error: " << data.to_string() << std::endl;
  //return { make_ping() };

  game_logic::msg_vector tmp;
  tmp.push_back(make_ping());
  return tmp;
}
